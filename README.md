# README #

### Changes ###
* Issues, enhancements, bugs, and other data is to be tracked in the Issues tracker and assigned to the appropriate user.

### Requirements ###

* BeautifulSoup4
* Selenium
* Python 2.7
* FireFox

### Contribution guidelines ###

* Inline documentation must be provided for final files.
* All code will be reviewed and approved by @notmike101
* All commits must describe what has been done.
    * If multiple changes per commit, describe what was done in the commit description.
* During the development stages, it is fine to commit directly to the master branch.  In later stages, such as testing, please fork the master branch and create pull requests.

### Who do I talk to? ###

Contact @notmike101 for any questions or concerns.

### Contributors ###
* @notmike101 (Michael Orozco) - Developer